<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no, user-scalable=no"/>
    <meta name="title" content="Appointment Booking">
    <title>Appointment Booking</title>
    @vite('resources/css/app.css')
</head>

<body>

<!--main content-->
<main class="d-flex min-vh-100">
    <div class="card uneek-wizard-card">
        <div class="card-body">

            <!--main content-->
            <form id="example-form" action="" method="post" novalidate>

                <div id="wizard">

                    {{--                    @include('sections.contact-information')--}}
                    @include('sections.general-information')


                    <!--step 3 steps-content-->
                    <h3>
                        <div class="media">
                            <div class="uneek-wizard-step-icon"><i class="zmdi zmdi-money"></i></div>
                            <div class="media-body">
                                <h5 class="uneek-wizard-step-title">Payment</h5>
                                <p class="uneek-wizard-step-subtitle">Lorem Ipsum is simply dummy text of the printing
                                    and typesetting industry.</p>
                            </div>
                        </div>
                    </h3>

                    <!--step 3 body-->
                    <section>
                        <div class="brand-wrapper"><img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo"
                                                        class="logo" width="120"></div>
                        <div class="row mb-3">
                            <div class="col-md-3 section-heading">Mode of Payment</div>
                            <div class="col-md-9 errorblock"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="customRadio1" name="pay" class="custom-control-input"
                                           value="During Visit" required>
                                    <label class="custom-control-label" for="customRadio1"> Pay during visit. </label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="customRadio2" name="pay" class="custom-control-input"
                                           required value="Paypal">
                                    <label class="custom-control-label" for="customRadio2"> Pay via Paypal. </label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="customRadio3" name="pay" class="custom-control-input"
                                           required value="Google-Pay">
                                    <label class="custom-control-label" for="customRadio3"> Pay using QR Code. </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!--paypal fields-->
                                <div class="Paypal box">
                                    <div class="form-group mb-2">
                                        <label for="payp">Paypal Address</label>
                                        <input type="email" name="payp" id="payp" class="form-control col-md-9 mb-1"
                                               placeholder="Paypal E-Mail" required>
                                        <p class="mb-0">Consultation Fees : <span id="xyz"></span></p>
                                        <p class="mb-0">Pay at : uneekcc1@gmail.com</p>
                                    </div>
                                </div>
                                <!--google pay fields-->
                                <div class="Google-Pay box">
                                    <div class="row">
                                        <div class="col-md-4 mb-3 text-center"><img
                                                src="{{ Vite::asset('resources/images/Uneekcc1.svg') }}" height="100px"
                                                alt="QR code for booking"></div>
                                        <div class="col-md-8 form-group">
                                            <label for="qrcode">Google Pay</label>
                                            <input type="text" name="qrcode" id="qrcode" class="form-control"
                                                   placeholder="Transaction ID">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="card-footer-text mt-3">Paying Online ? Please carry your payment receipt during your
                            visit.</p>
                    </section>

                    <!--step 4 steps-content-->
                    <h3>
                        <div class="media">
                            <div class="uneek-wizard-step-icon"><i class="zmdi zmdi-check-all"></i></div>
                            <div class="media-body">
                                <h5 class="uneek-wizard-step-title">Review</h5>
                                <p class="uneek-wizard-step-subtitle">Lorem Ipsum is simply dummy text of the printing
                                    and typesetting industry.</p>
                            </div>
                        </div>
                    </h3>

                    <!--step 4 body-->
                    <section>
                        <div class="brand-wrapper"><img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo"
                                                        class="logo" width="120"></div>
                        <div class="row">
                            <div class="col-xl-6">

                                <!--input data review-->
                                <div id="FormSummary">
                                    <div class="row mb-3">
                                        <div class="col-md-5 section-heading">Review details</div>
                                        <div class="col-md-7 errorblock"></div>
                                    </div>
                                    <h6 class="font-weight-bold"> Personal Details</h6>
                                    <p class="mb-0">Name : <span id="summaryfname"></span> <span
                                            id="summarylname"></span></p>
                                    <p class="mb-0">Contact : <span id="summarypnumber"></span></p>
                                    <p class="mb-0">Email : <span id="summaryemail"></span></p>
                                    <h6 class="font-weight-bold mt-3"> Appointment Details</h6>
                                    <p class="mb-0">Category : <span id="summarycategory"></span></p>
                                    <p class="mb-0">Service : <span id="summaryservice"></span></p>
                                    <p class="mb-0">Consultant : <span id="summaryconsultant"></span></p>
                                    <p class="mb-0">Additional Message : <span id="summarymessage"></span></p>
                                    <h6 class="font-weight-bold mt-3"> Payment Details</h6>
                                    <p class="mb-0">Mode of Payment : <span id="summarypayment"></span></p>
                                    <br>
                                </div>
                            </div>
                            <div class="col-xl-6 d-none d-xl-block"><img
                                    src="{{ Vite::asset('resources/images/step4.svg') }}" alt="booking"
                                    class="img-fluid"></div>
                        </div>
                        <p class="mb-0">We will contact you about the time slot on a particular day as per your
                            availability.</p>
                        <br>
                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input" name="tnc" id="tnc">
                            <label class="custom-control-label" for="tnc">I agree to the terms and conditions.</label>
                        </div>
                    </section>

                </div>

            </form>

        </div>
    </div>
</main>

@vite('resources/js/app.js')
</body>
</html>
