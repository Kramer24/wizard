<!--step 2 steps-content-->
<h3>
    <div class="media">
        <div class="uneek-wizard-step-icon"> <i class="zmdi zmdi-hospital"></i> </div>
        <div class="media-body">
            <h5 class="uneek-wizard-step-title">General information</h5>
            <p class="uneek-wizard-step-subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
    </div>
</h3>

<!--step 2 body-->
<section>
    <div class="brand-wrapper"> <img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo" class="logo" width="120"> </div>
    <div class="row mb-3">
        <div class="col-md-3 section-heading">Fill in general information</div>
        <div class="col-md-9 errorblock"></div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="logo"> Upload logo </label>
            <input type="file" id="logo" class="form-control" name="logo">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <label for="logo"> Site type </label>
            <div class="custom-control custom-radio mb-3">
                <input type="radio" id="typeShop" name="type" class="custom-control-input"
                       value="Shop" required>
                <label class="custom-control-label" for="customRadio1">Shop </label>
            </div>
            <div class="custom-control custom-radio mb-3">
                <input type="radio" id="typeLanding" name="type" class="custom-control-input"
                       required value="Landing">
                <label class="custom-control-label" for="customRadio2"> Landing </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <label for="category">Category</label>
            <select id="category" name="category" data-bind="select2:{} , options: togaMakers, value: selectedCategory,optionsValue:'text', optionsText : 'text', optionsCaption : 'Select your Category'" required>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="service">Service</label>
            <select id="service" name="service" data-bind="select2:{} ,options: togaServices, value: selectedService, optionsValue:'text', optionsText : 'text', optionsCaption : 'Select your Service', enable : togaServices" required>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="consultant">Consultant</label>
            <select id="consultant" name="consultant" data-bind="select2:{} ,options: togaConsultants, value: selectedConsultant,optionsValue:'text', optionsText : 'text', optionsCaption : 'Select your Consultant', enable: togaConsultants" required>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="dp">I'm available on or after</label>
            <input type="text" name="dp" class="datepicker-here form-control" data-language="en" id="dp" placeholder="Select Multiple Dates" data-date-format="dd M yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label for="message1" class="">Message</label>
        <textarea class="form-control" name="message1" id="message1" placeholder="Anything else?"></textarea>
    </div>
    <p class="card-footer-text">For different dates with different time, select date-1 and time-1, then date-2 and time-2 and so on.</p>
</section>
