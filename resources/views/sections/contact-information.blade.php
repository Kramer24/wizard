<!--step 1 steps-content-->
<h3>
    <div class="media">
        <div class="uneek-wizard-step-icon"> <i class="zmdi zmdi-assignment"></i> </div>
        <div class="media-body">
            <h5 class="uneek-wizard-step-title">Contact Information</h5>
            <p class="uneek-wizard-step-subtitle">Leave your details so we can contact you.</p>
        </div>
    </div>
</h3>

<!--step 1 body-->
<section>
    <!--header logo-->
    <div class="brand-wrapper"> <img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo" class="logo" width="120"> </div>
    <div class="row">
        <div class="col-xl-6">
            <!--heading step 1-->
            <div class="row mb-3">
                <div class="col-md-7 section-heading">Contact Information</div>
                <div class="col-md-5 errorblock"></div>
            </div>

            <!--form fields-->
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="fname" class="sr-only">First Name</label>
                    <input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="lname" class="sr-only">Last Name</label>
                    <input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name" required>
                </div>
            </div>
            <div class="form-group">
                <label for="pnumber" class="sr-only">Phone Number</label>
                <input type="tel"
                       {{--                                           pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" --}}
                       name="pnumber" id="pnumber" class="form-control" placeholder="Contact: 123-45-678" required>
            </div>
            <div class="form-group">
                <label for="email" class="sr-only">E-Mail</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Your E-Mail ID" required>
            </div>
        </div>

        <!--image step 1-->
        <div class="col-xl-6 d-none d-xl-block"> <img src="{{ Vite::asset('resources/images/step1.svg') }}" alt="appointment" class="img-fluid"> </div>
    </div>

    <!--card footer step 1-->
    <p class="card-footer-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
</section>
