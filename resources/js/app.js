import './bootstrap';

import './wizard/library/jquery3.5.1.min.js';
import './wizard/library/jquery-validation/dist/jquery.validate.min.js';
import './wizard/library/jquery-validation/dist/additional-methods.min.js';
import './wizard/library/bootstrap-4.4.1-dist/js/bootstrap.min.js';
import './wizard/library/jquery.steps.js';
import './wizard/library/air-datepicker-master/dist/js/datepicker.min.js';
import './wizard/library/air-datepicker-master/dist/js/i18n/datepicker.en.js';
import './wizard/library/select2-4.0.13/dist/js/select2.min.js';
import './wizard/library/knockout-min.js';
import './wizard/js/spreadsheet-js.js';
import './wizard/js/main.js';
